<?php
$e_activemenu = $this->action->id;
$controllers_ac = $this->id;
$session=new CHttpSession;
$session->open();
$login_member = $session['login_member'];

$active_menu_pg = $controllers_ac.'/'.$e_activemenu;
    $model = new ContactForm;
    $model->scenario = 'insert';

    if(isset($_POST['ContactForm']))
    {
      $model->attributes=$_POST['ContactForm'];

      if($model->validate())
      {
		  $secret_key = "6LcipigUAAAAAGtfEZKdbKi4yMrf86w0hQvpbM98";
		$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
		$response = json_decode($response);
		if($response->success==false)
		{
			$model->addError('verifyCode','Pastikan anda sudah menyelesaikan captcha');
		}else{
			// config email
			$messaged = $this->renderPartial('//mail/contact2',array(
			'model'=>$model,
			),TRUE);
			$config = array(
			'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
			'subject'=>'['.Yii::app()->name.'] Contact from '.$model->email,
			'message'=>$messaged,
			);
			if ($this->setting['contact_cc']) {
			$config['cc'] = array($this->setting['contact_cc']);
			}
			if ($this->setting['contact_bcc']) {
			$config['bcc'] = array($this->setting['contact_bcc']);
			}
			// kirim email
			Common::mail($config);

			Yii::app()->user->setFlash('success_mail','Thank you for contact us. We will respond to you as soon as possible.');
			$this->refresh();
		} 
      }

    }

?>
<section class="block_bottom_home">
	<div class="prelatife container insides maw1070">
		<div class="d-inline padding-right-15 w247">
			<div class="picts">
			<img src="<?php echo $this->assetBaseurl ?>cust_aldo_hum.png" alt="" class="img-responsive">
				</div>
		</div>
		<div class="d-inline padding-top-15 padding-left-10">
			<div class="clear height-20"></div>
			<div class="tops_tx">
				<div class="d-inline padding-right-15">
					<img src="<?php echo $this->assetBaseurl ?>bck_t_helped.png" alt="" class="img-responsive">
				</div>
				<div class="d-inline">
					<p>Hubungi Asisten Personal Anda</p>
					<ul class="list-inline">
						<li><img src="<?php echo $this->assetBaseurl ?>bt_icon_phones.png" alt="" class="d-inline padding-right-10"><?php echo $this->setting['contact_phone'] ?></li>
						<li><a href="mailto:<?php echo $this->setting['email'] ?>"><i class="fa fa-envelope padding-right-10"></i>Email Kami</a></li>
						<?php if ($this->setting['url_facebook'] != ''): ?>
						<li><a href="<?php echo $this->setting['url_facebook'] ?>" target="_blank"><i class="fa fa-facebook-square padding-right-10"></i>Facebook</a></li>
						<?php endif ?>
						<?php if ($this->setting['url_twitter'] != ''): ?>
						<li><a href="<?php echo $this->setting['url_twitter'] ?>" target="_blank"><i class="fa fa-twitter padding-right-10"></i>Twitter</a></li>
						<?php endif ?>
						<?php if ($this->setting['url_instagram'] != ''): ?>
						<li><a href="<?php echo $this->setting['url_instagram'] ?>" target="_blank"><i class="fa fa-instagram padding-right-10"></i>Instagram</a></li>
						<?php endif ?>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="btm_tx">
				<p>Anda juga dapat meninggalkan kontak anda, dan Asisten Personal Anda akan menghubungi anda segera...</p>
				<div class="clear"></div>
				<div class="bxs_form prelatife">
					<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		                'type'=>'inline',
		                'enableAjaxValidation'=>false,
		                'clientOptions'=>array(
		                    'validateOnSubmit'=>false,
		                ),
		                'htmlOptions' => array(
		                    'enctype' => 'multipart/form-data',
		                ),
		            )); ?>
		              <?php if(Yii::app()->user->hasFlash('success_mail')): ?>
		                  <?php /*$this->widget('bootstrap.widgets.TbAlert', array(
		                      'alerts'=>array('success'),
		                  ));*/ ?>
		                  <script type="text/javascript">
		                    alert("Thank you for sent contact! we will respond to you shortly."); 
		                  </script>
		              <?php endif; ?>
		              <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
					  <div class="form-group">
					  	<?php echo $form->textField($model, 'name', array('class'=>'form-control', 'required'=>'required', 'placeholder'=>'Nama')); ?>
					  </div>
					  <div class="form-group">
					  	<?php echo $form->textField($model, 'phone', array('class'=>'form-control', 'required'=>'required', 'placeholder'=>'Telepon')); ?>
					  </div>
					  <div class="form-group">
					  	<?php echo $form->textField($model, 'email', array('class'=>'form-control', 'required'=>'required', 'placeholder'=>'Email')); ?>
					  </div>
					  <div class="pos_captcha_fix_abs">
							<div class="g-recaptcha" data-sitekey="6LcipigUAAAAANVefMkroGu6ja3cmxVbpYUBh7zJ"></div>
							<script src='https://www.google.com/recaptcha/api.js'></script>
						</div>
					  <button type="button" class="btn btn-default buttons_subNewslet_frm">OK</button>
					<?php $this->endWidget(); ?>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear height-5"></div>
			<div class="bx_text social">
				<span class="d-inline padding-right-15">Terhubung dengan kami di</span>&nbsp;&nbsp;
				<?php if ($this->setting['url_facebook'] != ''): ?>
				<a href="<?php echo $this->setting['url_facebook'] ?>" target="_blank"> <i class="fa fa-facebook-square"></i> Facebook</a>&nbsp;&nbsp;&nbsp;
				<?php endif ?>
				<?php if ($this->setting['url_twitter'] != ''): ?>
				<a href="<?php echo $this->setting['url_twitter'] ?>" target="_blank"> <i class="fa fa-twitter"></i> Twitter</a>&nbsp;&nbsp;&nbsp;
				<?php endif ?>
				<?php if ($this->setting['url_instagram'] != ''): ?>
				<a href="<?php echo $this->setting['url_instagram'] ?>" target="_blank"> <i class="fa fa-instagram"></i> Instagram</a>
				<?php endif ?>
			</div>
			<div class="clear height-25"></div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</section>


<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('parent_id = 0');
$criteria->addCondition('type = "category"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->order = 'sort ASC';
$dataCategory = PrdCategory::model()->findAll($criteria);
if (count($dataCategory) > 0) {
	$dataCategory = array_chunk($dataCategory, ceil(count($dataCategory) / 2));
}
?>
<footer class="foot">
	<div class="prelatife container"> 
		<div class="clear height-30"></div>
		<div class="insides_footer">
			<div class="row">
				<div class="col-md-3">
					<div class="lgo_footers_wb"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl ?>lgo_footers_web_aldo.jpg" alt="" class="img-responsive"></a></div>
				</div>
				<div class="col-md-6 col-sm-8">
					<span class="block_b">kategori PRODUK</span>
					<div class="clear height-10"></div>
					<div class="row">
					<?php if ($dataCategory): ?>
						<?php foreach ($dataCategory as $v): ?>
							<div class="col-sm-6 col-md-6">
								<ul class="list-unstyled">
								<?php foreach ($v as $key => $value): ?>
									<li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a></li>
								<?php endforeach ?>
								</ul>
							</div>
						<?php endforeach ?>
					<?php endif ?>
					</div>
				</div>
				<div class="col-md-3 col-sm-4">
					<span class="block_b">kategori PRODUK</span>
					<div class="clear height-10"></div>
					<ul class="list-unstyled">
						<li><a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">Login / Logout</a></li>
						<li><a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">Daftar</a></li>
					</ul>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="tops_footer">
	<div class="prelatife container back-cream">
		<div class="maw1087 tengah text-center">
		<p>ALDO online hardware & tools shop adalah toko elektronik retail dengan jaringan toko yang tersebar di berbagai penjuru strategis di Indonesia. Dengan memanfaatkan cakupan distribusi dan jaringan pemasaran yang luas, ALDO hardware & tools mampu menghadirkan layanan yang menguntungkan pelanggan, mulai dari harga yang sangat kompetitif hingga distribusi yang strategis! ALDO hardware & tools menjual aneka barang keperluan pertukangan, industri, otomotif, keperluan rumah, hingga hotel dan masih banyak lagi. Mari berbelanja di Aldo online hardware & tools dan temukan produk yang anda inginkan dengan harga yang terbaik dan sangat bersaing!</p>
		</div>
		<div class="clear"></div>
	</div>
</div>
	<div class="backs_blue">
		<div class="prelatife container text-center">
			<p class="t-copyrights">
				Copyright &copy; ALDO 2017 - Website design by <a href="http://www.markdesign.net/" target="_blank" title="Website Design Surabaya">Mark Design.</a>
			</p>
		</div>
	</div>
</footer>
<script type="text/javascript">
	$(document).ready(function(){

		$('.pos_captcha_fix_abs .g-recaptcha').hide();

		$('button[type=button].buttons_subNewslet_frm').live('click', function(){
			$(this).prop('type', 'submit');
			$('.pos_captcha_fix_abs .g-recaptcha').show();
			return false;
		});
	});
</script>
<style>
	.pos_captcha_fix_abs {
		position: absolute;
		left: 10px;
		bottom: -85px;
		z-index: 50;
	}

	<?php if($active_menu_pg == 'home/pcontact'): ?>
	section.block_bottom_home{
		max-height: 30px;
		min-height: 5px;
	}
	section.block_bottom_home .container.insides{ display: none; }
	<?php endif; ?> 
</style>