<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;

$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('parent_id = 0');
$criteria->addCondition('type = "category"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->order = 'sort ASC';
$dataCategory = PrdCategory::model()->findAll($criteria);
?>

<div class="outers_back_headers">
  <header class="head">
    <div class="visible-lg visible-md">
      <div class="tops prelatife container">
        <div class="row">
          <div class="col-md-4">
            <div class="clear height-10"></div>
            <div class="lgo_web_header d-inline">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                <img src="<?php echo $this->assetBaseurl ?>lgo_header_web_n1.jpg" alt="" class="img-responsive">
              </a>
            </div>
            <div class="taglineshdr_lefts d-inline padding-top-20">
              <img src="<?php echo $this->assetBaseurl ?>tx_tools_hardwar_header.png" alt="" class="img-responsive">
            </div>
          </div>
          <div class="col-md-8">
            <div class="rights_header prelatife">
                <div class="d-inline b_search">
                  <form action="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>" method="get">
                    <div class="form-group prelatife">
                      <input type="text" class="form-control" name="q" value="<?php echo $_GET['q'] ?>" placeholder="Cari">
                      <button type="submit" class="btn btn-link">
                        <i class="fa fa-search"></i>
                      </button>
                    </div>
                  </form>
                  <div class="clear"></div>
                </div>

                <div class="d-inline boxs_account_loginHeader">
                  <?php if ($login_member == null): ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                    <i class="fa fa-bg_account"></i> &nbsp;
                    <span>Login</span>
                  </a>
                  <?php else: ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                    <i class="fa fa-bg_account"></i> &nbsp;
                    <span>Akun Saya</span>
                  </a>
                  <?php endif ?>
                  <div class="clear"></div>
                </div>
                <div class="d-inline frights back-grey boxs_bCart">
                  <a href="<?php echo CHtml::normalizeUrl(array('/cart/shop')); ?>">
                    <i class="fa fa-carts_bag"></i> &nbsp;<span><?php echo Cart::getTotalCartItem() ?></span>
                  </a>
                  <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>
          </div>
        <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>
    <div class="bottoms">
      <div class="prelatife">
        <div class="top-menu">
          <ul class="list-inline">
            <li><a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">PRODUK ALDO</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/promosi')); ?>">PROMOSI</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">ABOUT US</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">TIPS & SARAN</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/lokasitoko')); ?>">LOKASI PENJUALAN KAMI</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">HUBUNGI KAMI</a></li>
          </ul>
          <div class="clear clearfix"></div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
    </div>
    <!-- end bottom -->

      <div class="visible-sm visible-xs">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                <img src="<?php echo $this->assetBaseurl ?>lgo_header_web_n1.jpg" alt="" class="img-responsive">
                &nbsp;&nbsp;Tools & Hardware
              </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">PRODUK ALDO</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/promosi')); ?>">PROMOSI</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">ABOUT US</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/tips')); ?>">TIPS & SARAN</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/lokasitoko')); ?>">LOKASI PENJUALAN KAMI</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">HUBUNGI KAMI</a></li>
              </ul>
              <div class="clear height-5"></div>
              <div class="bloc_aldoHeader">
                  <?php if ($login_member == null): ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                    <span>Login</span>
                  </a>
                  <?php else: ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                    <span>Akun Saya</span>
                  </a>
                  <?php endif ?>
                  <div class="boxs_bCart">
                    <a href="<?php echo CHtml::normalizeUrl(array('/cart/shop')); ?>">
                      <i class="fa fa-shopping-cart"></i> &nbsp;<span>My Cart</span>
                    </a>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
              </div>

            </div>
          </div>
        </nav>
        <div class="clear"></div>
      </div>

    <div class="clear"></div>
  </header>
</div>

<?php /*foreach($dataCategory as $key => $value): ?>
            <li>
              <a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
              <div class="blocks_dropDown_menu_fx">
                <div class="row">
  <?php
  $criteria = new CDbCriteria;
  $criteria->with = array('description');
  $criteria->addCondition('parent_id = :parent');
  $criteria->addCondition('type = "category"');
  $criteria->addCondition('description.language_id = :language_id');
  $criteria->params[':language_id'] = $this->languageID;
  $criteria->params[':parent'] = $value->id;
  $criteria->order = 'sort ASC';
  $dataCategory2 = PrdCategory::model()->findAll($criteria);
  ?>
                  <?php foreach ($dataCategory2 as $k => $val): ?>
                    
                  <div class="col-md-2 col-sm-2">
                    <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$val->id)); ?>"><h5 class="subs_mn"><?php echo $val->description->name ?></h5></a>
                    <div class="clear"></div>
                    <ul class="list-unstyled">
  <?php
  $criteria = new CDbCriteria;
  $criteria->with = array('description');
  $criteria->addCondition('parent_id = :parent');
  $criteria->addCondition('type = "category"');
  $criteria->addCondition('description.language_id = :language_id');
  $criteria->params[':language_id'] = $this->languageID;
  $criteria->params[':parent'] = $val->id;
  $criteria->order = 'sort ASC';
  $dataCategory3 = PrdCategory::model()->findAll($criteria);
  ?>
                      <?php foreach ($dataCategory3 as $v): ?>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><?php echo $v->description->name ?></a></li>
                      <?php endforeach ?>
                    </ul>
                  </div>
                  <?php if (($k + 1) % 6 == 0): ?>
                    <div class="clear"></div>
                  <?php endif ?>
                  <?php endforeach ?>
                </div>
              </div>
            </li>
            <?php endforeach ?>
*/ ?>

<?php /*
<script type="text/javascript">
  $(document).ready(function(){
      $('.nl_popup a').live('hover', function(){
          $('.popup_carts_header').fadeIn();
      });
      $('.popup_carts_header').live('mouseleave', function(){
        setTimeout(function(){ 
            $('.popup_carts_header').fadeOut();
        }, 500);
      });
  });
</script>

<script type="text/javascript">
    $(function(){
      $('#myAffix').affix({
        offset: {
          top: 300
        }
      })
    })
  </script>

<section id="myAffix" class="header-affixs affix-top">
  <div class="clear height-5"></div>
  <div class="prelatife container">
    <div class="row">
      <div class="col-md-3 col-sm-3">
        <div class="lgo-web-web_affix">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
            <img src="<?php echo $this->assetBaseurl ?>lgo_stumble_wt_txt.png" alt="" class="img-responsive d-inline" style="max-width: 75px;">
            STUMBLE UPON
          </a>
        </div>
      </div>
      <div class="col-md-9 col-sm-9">
        <div class="text-right"> 
          <div class="clear height-20"></div>
          <div class="menu-taffix">
            <ul class="list-inline">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>">BUY COFFEE</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/howto')); ?>">HOW TO ORDER</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/wholesale')); ?>">WHOLESALE</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">ABOUT US</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>">EVENTS</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT US</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</section>
*/ ?>