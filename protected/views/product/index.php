<div class="clear"></div>
<div class="subpage defaults_static">
  <div class="top_title_page margin-bottom-40">
    <div class="prelatife container">
      <div class="row default">
        <div class="col-md-4">
              <?php if ($strCategory != null): ?>

<?php
$bread = PrdCategory::model()->getBreadcrump($_GET['category'], $this->languageID);
$bread = array_reverse($bread,true);
$dataCategory = array();
foreach ($bread as $key => $value) {
  // $this->breadcrumbs[$key]=$value;
  array_push($dataCategory, $key);
}
$dataCategory = array_reverse($dataCategory);
?>

                <?php if ($_GET['q'] != ''): ?>
                <h2 class="title_pg">Cari "<?php echo $_GET['q'] ?>" di kategori <?php echo implode(' -> ', $dataCategory) ?></h2>
                <?php else: ?>
                <h2 class="title_pg">Kategori <?php echo implode(' -> ', $dataCategory) ?></h2>
                <?php endif ?>
              <?php elseif($_GET['q'] != ''): ?>
              <h2 class="title_pg">Cari "<?php echo $_GET['q'] ?>" di Semua Produk</h2>
              <?php else: ?>
              <h2 class="title_pg">Cari Semua Produk</h2>
              <?php endif ?>
        </div>
<?php
$data = $product->getData();
?>
<?php
$get = $_GET;
// if ($_GET['category'] != '') {
//   $get['category'] = $_GET['category'];
// }
unset($get['order']);
?>

        <div class="col-md-8">
          <div class="flot_filter_top_productPg">
            <div class="d-inline block_itm">
            <form action="<?php echo $this->createUrl('/product/index', $get) ?>" method="get" id="form-filter">
                <div class="form-group">
                <label for="">Sortir berdasar</label>
                <select name="order" id="select-order" class="form-control select-filter">
                  <option value="new">Terbaru</option>
                  <option value="low-price">Harga Terendah</option>
                  <option value="hight-price">Harga Tetinggi</option>
                </select>
                </div>
            </form>
            </div>
            <div class="d-inline block_itm filter_pagin">
              Tampilkan&nbsp;&nbsp;&nbsp;&nbsp;   
              <?php $getPagination = $_GET; ?>
              <?php $getPagination['page_size'] = 16; ?>
              <a href="<?php echo $this->createUrl('/product/index', $getPagination) ?>">16</a>&nbsp;&nbsp;|&nbsp;&nbsp;
              <?php $getPagination['page_size'] = 32; ?>
              <a href="<?php echo $this->createUrl('/product/index', $getPagination) ?>">32</a>&nbsp;&nbsp;|&nbsp;&nbsp;
              <?php $getPagination['page_size'] = 96; ?>
              <a href="<?php echo $this->createUrl('/product/index', $getPagination) ?>">96</a>
            </div>
            <div class="d-inline block_itm filter_pagein2">
              Halaman <?php echo $product->pagination->currentPage + 1 ?> dari <?php echo $product->pagination->getPageCount() ?>&nbsp;&nbsp;&nbsp;&nbsp;   
              <?php $getPage = $_GET; ?>
              <?php $getPage['PrdProduct_page'] = $product->pagination->currentPage; ?>
              <?php if ($product->pagination->currentPage > 0): ?>
              <a href="<?php echo $this->createUrl('/product/index', $getPage) ?>"><i class="fa fa-arrow-left"></i></a>
              <?php endif ?>
              <?php $getPage['PrdProduct_page'] = $product->pagination->currentPage + 2; ?>
              <?php if ($product->pagination->currentPage > 0 AND $product->pagination->getPageCount() >= $getPage['PrdProduct_page']): ?>
              &nbsp;&nbsp;|&nbsp;&nbsp;
              <?php endif ?>
              <?php if ($product->pagination->getPageCount() >= $getPage['PrdProduct_page']): ?>
              <a href="<?php echo $this->createUrl('/product/index', $getPage) ?>"><i class="fa fa-arrow-right"></i></a>
              <?php endif ?>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div> <div class="clear"></div>
    </div>
  </div>
<script type="text/javascript">
$('.select-filter').on('change', function() {
  $('#form-filter').submit();
})
<?php if ($_GET['order'] != ''): ?>
$('#select-order').val('<?php echo $_GET['order'] ?>');
  
<?php endif ?>
</script>

  <div class="middle inside_content">
    <div class="prelatife container">
      
      <!-- Start data default product -->
      <div class="lists_product_data row">
        <?php foreach ($data as $key => $value): ?>
        <div class="col-md-3 col-sm-6">
          <div class="items">
            <div class="picture prelatife">
              <?php if ($value->rekomendasi == 1): ?>
                <div class="boxs_inf_head_n1 back2"></div>
              <?php elseif ($value->onsale == 1): ?>
                <div class="boxs_inf_head_n1"></div>
              <?php else: ?>

              <?php endif ?>
              <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
              <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(321,321, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img-responsive" alt="">
              </a>
            </div>
            <div class="info description">
            <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
              <p><?php echo $value->description->name ?></p>
              <div class="block_price">
                <?php if ($value->harga_coret > $value->harga): ?>
                <span class="price_trough">Harga Normal <i><?php echo Cart::money($value->harga_coret) ?></i></span>
                <div class="clear"></div>
                <span class="price">Kini <b><?php echo Cart::money($value->harga) ?></b></span>
                <div class="clear"></div>
                <span class="bottom_priceblack">Hemat RP <?php echo Cart::money($value->harga_coret - $value->harga) ?></span>
                <div class="clear"></div>
                <?php else: ?>
                <span class="price_trough">&nbsp;</span>
                <div class="clear"></div>
                <span class="price"><b><?php echo Cart::money($value->harga) ?></b></span>
                <div class="clear"></div>
                <span class="bottom_priceblack">&nbsp;</span>
                <div class="clear"></div>
                <?php endif ?>
              </div>
            </a>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <?php endforeach ?>
      </div>
      <?php $this->widget('CLinkPager', array(
        'pages' => $product->getPagination(),
        'header' => '',
        'htmlOptions' => array('class' => 'pagination'),
      )) ?>
      <!-- End data default product -->
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>









