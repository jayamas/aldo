<?php
$session=new CHttpSession;
$session->open();
$login_member = $session['login_member'];
?>
<?php
$bread = PrdCategory::model()->getBreadcrump($data->category_id, $this->languageID);
$bread = array_reverse($bread,true);
$dataCategory = array();
foreach ($bread as $key => $value) {
  // $this->breadcrumbs[$key]=$value;
  array_push($dataCategory, $key);
}
$dataCategory = array_reverse($dataCategory);
?>
<script src="<?php echo Yii::app()->baseUrl ?>/asset/js/sweetalert/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ?>/asset/js/sweetalert/sweetalert.css">

<div class="clear"></div>
<div class="subpage defaults_static def_productDetail">
  <div class="top_title_page margin-bottom-20">
    <div class="prelatife container">
      <div class="row">
        <div class="col-md-10 col-sm-10">
          <h2 class="title_pg"><?php echo $data->description->name ?></h2>
        </div>
        <div class="col-md-2 col-sm-2 text-right">
          <a href="#" onclick="window.history.back();" class="btn btn-link bloc_backProduct_list"><i class="fa fa-arrow-left"></i> &nbsp;Kembali</a>
        </div>
      </div>
    </div>
  </div>

  <div class="middle inside_content">
    <div class="prelatife container">
      <div class="bloc_detail_products">
        <div class="row default_detail_p">
          <div class="col-md-6">
            <div class="detc_picture_big table_out">
              <div class="table_in">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(500,500, '/images/product/'.$data->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="rights_cont">
              <?php if ($value->rekomendasi == 1): ?>
                <div class="tops_c_bannerSale"><span>Rekomendasi!</span></div>
              <?php elseif ($value->onsale == 1): ?>
                <div class="tops_c_bannerSale"><span>ON SALE!</span></div>
              <?php else: ?>

              <?php endif ?>
              <div class="clear height-10"></div>
              <div class="bloctn_detailtop">
                <div class="bloc1">
                  <h2><?php echo $data->description->name ?></h2>
                  <h5>Kategori <?php echo implode(' -> ', $dataCategory) ?></h5>
                </div>
                <div class="bloc2 border-top block_price">
                  <?php if ($data->harga_coret > $data->harga): ?>
                  <span class="price_trough">Harga Normal <i><?php echo Cart::money($data->harga_coret) ?></i></span>
                  <div class="clear"></div>
                  <span class="price">Kini <b><?php echo Cart::money($data->harga) ?></b></span>
                  <div class="clear"></div>
                  <span class="bottom_priceblack">Hemat RP <?php echo Cart::money($data->harga_coret - $data->harga) ?></span>
                  <div class="clear"></div>
                  <?php else: ?>
                  <span class="price_trough">&nbsp;</span>
                  <div class="clear"></div>
                  <span class="price"><b><?php echo Cart::money($data->harga) ?></b></span>
                  <div class="clear"></div>
                  <span class="bottom_priceblack">&nbsp;</span>
                  <div class="clear"></div>
                  <?php endif ?>
                </div>
                <?php if(Yii::app()->user->hasFlash('success')): ?>

<script type="text/javascript">
swal({
  title: "<?php echo Yii::app()->user->getFlash('success') ?>",
  text: "Do you want to continue shopping?",
  type: "success",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "yes, Continue Shopping",
  cancelButtonText: "Go to Cart",
  // closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {
    
  } else {
    window.location.href = '<?php echo CHtml::normalizeUrl(array('/cart/shop')); ?>';
  }
});
</script>
                <?php /*
                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
                        'alerts'=>array('success'),
                    )); ?>
                */ ?>
                <?php endif; ?>

                <?php if(Yii::app()->user->hasFlash('danger')): ?>
                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
                        'alerts'=>array('danger'),
                    )); ?>
                <?php endif; ?>
                <div class="bloc3 back-white boxs_formn_buying">
                <form class="form-inline" action="<?php echo CHtml::normalizeUrl(array('addcart')); ?>" method="post">
                    <input type="hidden" id="price-item" value="<?php echo $data->harga ?>">
                    <input type="hidden" name="id" value="<?php echo $data->id ?>">
                    <div class="form-group">
                      <label>Jumlah pembelian</label>
                    </div>
                    <div class="form-group">
                      <input type="number" name="qty" class="form-control" value="1">
                    </div>
                    <button type="submit" class="btn btn-default btns_bckOrange">TAMBAH KE KERANJANG BELANJA</button>
                  </form>
                </div>
                <div class="clear"></div>
                <div class="bottoms_descriptions padding-top-25">
                  <p class="mb-0"><strong>Keterangan produk:</strong></p>
                  <?php echo $data->description->desc ?>
                  <div class="clear"></div>
                </div>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="clear height-20"></div>
<?php
$criteria=new CDbCriteria;
$criteria->with = array('description', 'category', 'brand');
$criteria->order = 'date DESC';
$criteria->addCondition('status = "1"');
$criteria->addCondition('category_id = :cat');
$criteria->params[':cat'] = $data->category_id;
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $pageSize = 12;
$criteria->group = 't.id';
$dataProduct = PrdProduct::model()->findAll($criteria);

?>      <section class="default_sc blocks_home2 padding-0">
        <div class="block_product_data_wrap">
          <div class="top">
            <h6>Simak Produk <?php echo $data->category->description->name ?> Terbaru</h6>
          </div>
          <div id="owl-demo" class="lists_product_data row">
            <?php foreach ($dataProduct as $key => $value): ?>
            <div class="col-md-12">
              <div class="items">
                <div class="picture prelatife">
                  <?php if ($value->rekomendasi == 1): ?>
                    <div class="boxs_inf_head_n1 back2"></div>
                  <?php elseif ($value->onsale == 1): ?>
                    <div class="boxs_inf_head_n1"></div>
                  <?php else: ?>

                  <?php endif ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(321,321, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img-responsive" alt="">
                  </a>
                </div>
                <div class="info description">
                <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
                  <p><?php echo $value->description->name ?></p>
                  <div class="block_price">
                    <?php if ($value->harga_coret > $value->harga): ?>
                    <span class="price_trough">Harga Normal <i><?php echo Cart::money($value->harga_coret) ?></i></span>
                    <div class="clear"></div>
                    <span class="price">Kini <b><?php echo Cart::money($value->harga) ?></b></span>
                    <div class="clear"></div>
                    <span class="bottom_priceblack">Hemat RP <?php echo Cart::money($value->harga_coret - $value->harga) ?></span>
                    <div class="clear"></div>
                    <?php else: ?>
                    <span class="price_trough">&nbsp;</span>
                    <div class="clear"></div>
                    <span class="price"><b><?php echo Cart::money($value->harga) ?></b></span>
                    <div class="clear"></div>
                    <span class="bottom_priceblack">&nbsp;</span>
                    <div class="clear"></div>
                    <?php endif ?>
                  </div>
                </a>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
            <?php endforeach ?>
          </div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </section>

      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js" type="text/javascript"></script>

<script type="text/javascript">
// $.noConflict();
$(document).ready(function() {
  var owl = $("#owl-demo, #owl-demo2");
 
  owl.owlCarousel({
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [768, 2], //2 items between 600 and 0
      itemsMobile : [600, 1], // itemsMobile disabled - inherit from itemsTablet option
      pagination: true,
      paginationNumbers: true
  });

  $(".play").click(function(){
    owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
  })

});
</script>
<style type="text/css">
  .owl-carousel .owl-wrapper-outer{
    width: 99.6%;
  }
</style>