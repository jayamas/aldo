<div class="clear height-50"></div>
<section class="default_sc blocks_hom1 back-white">
	<div class="prelatife container">
		<?php /*<div class="lists_banner_home_dt">
			<div class="row">
				<div class="col-md-3">
					<div class="items prelatife">
						<div class="info">
							<h3>Aneka Peralatan Resto</h3>
							<span>Di bawah 1 jutaan</span>
						</div>
						<div class="pict rights">
							<img src="<?php echo $this->assetBaseurl ?>pic_bottom_fcs_d-1.jpg" alt="" class="img-responsive">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="items prelatife">
						<div class="info">
							<h3>Safety Equipment</h3>
							<span>Diskon Hingga <br><strong>Rp 10%</strong></span>
						</div>
						<div class="pict rights">
							<img src="<?php echo $this->assetBaseurl ?>pic_bottom_fcs_d-2.jpg" alt="" class="img-responsive">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="items prelatife">
						<div class="info">
							<h3>Baru! Perlengkapan Taman</h3>
							<span>Di bawah 500ribu</span>
						</div>
						<div class="pict rights">
							<img src="<?php echo $this->assetBaseurl ?>pic_bottom_fcs_d-3.jpg" alt="" class="img-responsive">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="items prelatife">
						<div class="info">
							<h3>Harga Gila Kotak Perkakas</h3>
							<span class="price">
								<i>Rp 79.000</i><br>
								Rp 79.000
							</span>
						</div>
						<div class="pict rights">
							<img src="<?php echo $this->assetBaseurl ?>pic_bottom_fcs_d-4.jpg" alt="" class="img-responsive">
						</div>
					</div>
				</div>

			</div>
		</div>*/ ?>
		<div class="lists_banner_home_dt new_picture">
			<div class="row">
			<?php for ($i=1; $i < 5; $i++) { ?>
				<div class="col-md-3 col-sm-6">
					<div class="items prelatife">
						<div class="pict">
							<a href="<?php echo $this->setting['home_banner_url_'.$i] ?>">
								<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(321,176, '/images/static/'.$this->setting['home_banner_image_'.$i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
							</a>
						</div>
					</div>
				</div>
			<?php } ?>
			</div>
		</div>
		<div class="clear height-15"></div>

		<div class="banner_full_pic middle_thome">
			<div class="row">
			<?php for ($i=5; $i < 7; $i++) { ?>
				<div class="col-md-6">
					<a href="<?php echo $this->setting['home_banner_url_'.$i] ?>">
						<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(654,202, '/images/static/'.$this->setting['home_banner_image_'.$i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
					</a>
				</div>
			<?php } ?>
			</div>
		</div>

		<div class="clear height-40"></div>
		<div class="clear"></div>
	</div>
</section>
<?php
$criteria=new CDbCriteria;
$criteria->with = array('description', 'category', 'brand');
$criteria->order = 'date DESC';
$criteria->addCondition('status = "1"');
$criteria->addCondition('terlaris = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $pageSize = 12;
$criteria->group = 't.id';
$dataProduct = PrdProduct::model()->findAll($criteria);

?>
<section class="default_sc blocks_home2 back-orange">
	<div class="prelatife container">

		<div class="block_product_data_wrap">
			<div class="top">
				<h6>Produk Terpopular</h6>
			</div>
			<div id="owl-demo" class="lists_product_data row">
				<?php foreach ($dataProduct as $key => $value): ?>
				<div class="col-md-12">
					<div class="items">
						<div class="picture prelatife">
							<?php if ($value->rekomendasi == 1): ?>
								<div class="boxs_inf_head_n1 back2"></div>
							<?php elseif ($value->onsale == 1): ?>
								<div class="boxs_inf_head_n1"></div>
							<?php else: ?>

							<?php endif ?>
							<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
							<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(321,321, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img-responsive" alt="">
							</a>
						</div>
						<div class="info description">
						<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
							<p><?php echo $value->description->name ?></p>
							<div class="block_price">
								<?php if ($value->harga_coret > $value->harga): ?>
								<span class="price_trough">Harga Normal <i><?php echo Cart::money($value->harga_coret) ?></i></span>
								<div class="clear"></div>
								<span class="price">Kini <b><?php echo Cart::money($value->harga) ?></b></span>
								<div class="clear"></div>
								<span class="bottom_priceblack">Hemat RP <?php echo Cart::money($value->harga_coret - $value->harga) ?></span>
								<div class="clear"></div>
								<?php else: ?>
								<span class="price_trough">&nbsp;</span>
								<div class="clear"></div>
								<span class="price"><b><?php echo Cart::money($value->harga) ?></b></span>
								<div class="clear"></div>
								<span class="bottom_priceblack">&nbsp;</span>
								<div class="clear"></div>
								<?php endif ?>
							</div>
						</a>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<?php endforeach ?>
			</div>
			<div class="clear"></div>
		</div>

<?php
$criteria=new CDbCriteria;
$criteria->with = array('description', 'category', 'brand');
$criteria->order = 'date DESC';
$criteria->addCondition('status = "1"');
$criteria->addCondition('terbaru = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $pageSize = 12;
$criteria->group = 't.id';
$dataProduct = PrdProduct::model()->findAll($criteria);

?>

		<div class="block_product_data_wrap">
			<div class="top">
				<h6>Produk Koleksi Terbaru</h6>
			</div>
			<div id="owl-demo2" class="lists_product_data row">
				<?php foreach ($dataProduct as $key => $value): ?>
				<div class="col-md-12">
					<div class="items">
						<div class="picture prelatife">
							<?php if ($value->rekomendasi == 1): ?>
								<div class="boxs_inf_head_n1 back2"></div>
							<?php elseif ($value->onsale == 1): ?>
								<div class="boxs_inf_head_n1"></div>
							<?php else: ?>

							<?php endif ?>
							<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
							<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(321,321, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img-responsive" alt="">
							</a>
						</div>
						<div class="info description">
						<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
							<p><?php echo $value->description->name ?></p>
							<div class="block_price">
								<?php if ($value->harga_coret > $value->harga): ?>
								<span class="price_trough">Harga Normal <i><?php echo Cart::money($value->harga_coret) ?></i></span>
								<div class="clear"></div>
								<span class="price">Kini <b><?php echo Cart::money($value->harga) ?></b></span>
								<div class="clear"></div>
								<span class="bottom_priceblack">Hemat RP <?php echo Cart::money($value->harga_coret - $value->harga) ?></span>
								<div class="clear"></div>
								<?php else: ?>
								<span class="price_trough">&nbsp;</span>
								<div class="clear"></div>
								<span class="price"><b><?php echo Cart::money($value->harga) ?></b></span>
								<div class="clear"></div>
								<span class="bottom_priceblack">&nbsp;</span>
								<div class="clear"></div>
								<?php endif ?>
							</div>
						</a>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<?php endforeach ?>
			</div>
			<div class="clear"></div>
		</div>
<?php
$criteria=new CDbCriteria;
$criteria->with = array('description', 'category', 'brand');
$criteria->order = 'date DESC';
$criteria->addCondition('status = "1"');
$criteria->addCondition('turun_harga = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $pageSize = 12;
$criteria->group = 't.id';
$dataProduct = PrdProduct::model()->findAll($criteria);

?>

		<div class="block_product_data_wrap">
			<div class="top">
				<h6>Produk Turun Harga</h6>
			</div>
			<div id="owl-demo2" class="lists_product_data row">
				<?php foreach ($dataProduct as $key => $value): ?>
				<div class="col-md-12">
					<div class="items">
						<div class="picture prelatife">
							<?php if ($value->rekomendasi == 1): ?>
								<div class="boxs_inf_head_n1 back2"></div>
							<?php elseif ($value->onsale == 1): ?>
								<div class="boxs_inf_head_n1"></div>
							<?php else: ?>

							<?php endif ?>
							<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
							<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(321,321, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img-responsive" alt="">
							</a>
						</div>
						<div class="info description">
						<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
							<p><?php echo $value->description->name ?></p>
							<div class="block_price">
								<?php if ($value->harga_coret > $value->harga): ?>
								<span class="price_trough">Harga Normal <i><?php echo Cart::money($value->harga_coret) ?></i></span>
								<div class="clear"></div>
								<span class="price">Kini <b><?php echo Cart::money($value->harga) ?></b></span>
								<div class="clear"></div>
								<span class="bottom_priceblack">Hemat RP <?php echo Cart::money($value->harga_coret - $value->harga) ?></span>
								<div class="clear"></div>
								<?php else: ?>
								<span class="price_trough">&nbsp;</span>
								<div class="clear"></div>
								<span class="price"><b><?php echo Cart::money($value->harga) ?></b></span>
								<div class="clear"></div>
								<span class="bottom_priceblack">&nbsp;</span>
								<div class="clear"></div>
								<?php endif ?>
							</div>
						</a>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<?php endforeach ?>
			</div>
			<div class="clear"></div>
		</div>

		
		<div class="clear"></div>
	</div>
</section>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js" type="text/javascript"></script>

<script type="text/javascript">
// $.noConflict();
$(document).ready(function() {
  var owl = $("#owl-demo, #owl-demo2");
 
  owl.owlCarousel({
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [768, 2], //2 items between 600 and 0
      itemsMobile : [600, 1], // itemsMobile disabled - inherit from itemsTablet option
  	  pagination: true,
  	  paginationNumbers: true
  });

  $(".play").click(function(){
    owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
  })

});
</script>
<style type="text/css">
  .owl-carousel .owl-wrapper-outer{
    width: 99.6%;
  }
</style>




<?php /*
<!-- conts box top -->
<section class="default_sc blocks_bannerBox_home" id="block_homesection">

	<div class="in_box_product back-white">
		<div class="tops yellow text-center">
			<h3 class="sub_title">Penawaran Istimewa &amp; Terbatas!</h3>
		</div>
<?php
$criteria=new CDbCriteria;
$criteria->with = array('description', 'category', 'brand');
$criteria->order = 'date DESC';
$criteria->addCondition('status = "1"');
$criteria->addCondition('terlaris = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $pageSize = 12;
$criteria->group = 't.id';
$dataProduct = PrdProduct::model()->findAll($criteria);

?>
		<div class="prelatife container">
			<div class="insides padding-top-15">
				<div id="owl-demo" class="lists_product_data owl-carousel owl-theme">
					<?php foreach ($dataProduct as $key => $value): ?>
						
					<div>
						<div class="items prelatife">
							<div class="t_info prelatife">
								<div class="row">
									<div class="col-xs-4 lgo_brands">
										<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(100,35, '/images/brand/'.$value->brand->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="">
									</div>
									<div class="col-xs-8">
										<div class="blocks_recom">
											<?php if (1 == $value->terbaru): ?>
											<img src="<?php echo $this->assetBaseurl ?>backs_con_right_top_products-yellow2.png" alt="">	
											<?php elseif($value->harga_coret > $value->harga): ?>
											<img src="<?php echo $this->assetBaseurl ?>backs_con_right_top_products2.png" alt="">
											<?php endif ?>
										</div>
									</div>
								</div>
							</div>
							<div class="b_titles">
								<?php echo $value->description->name ?>
							</div>
							<div class="picture">
								<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
								<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(250,250, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block">
								</a>
							</div>
							<div class="bloc_bottom">
								<div class="row">
									<div class="col-xs-9">
										<div class="prices">
											<?php if ($value->harga_coret > $value->harga): ?>
											<span class="through">Harga Normal <i><?php echo Cart::money($value->harga_coret) ?></i></span>
											<span>Kini <b><?php echo Cart::money($value->harga) ?></b></span>
											<small>Hemat <?php echo Cart::money($value->harga_coret - $value->harga) ?></small>
											<?php else: ?>
											<span class="through">&nbsp;</span>
											<span><b><?php echo Cart::money($value->harga) ?></b></span>
											<small>&nbsp;</small>
											<?php endif ?>
										</div>
									</div>
									<div class="col-xs-3 text-right">
										<?php
										$dataSerial = unserialize($value->data);
										?>
										<?php if ($dataSerial['feature'] != ''): ?>
										<i class="infree_shipping"></i>
										<?php endif ?>
										<div class="clear"></div>
										<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>" class="btn btn-link btns_blueProducts"><i class="fa fa-search"></i></a>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<?php endforeach ?>
				</div>
				<!-- end list product data -->
				<!-- <div class="text-center bs_bottom_pagin">
					<p>Halaman 1 dari 3</p>
				</div> -->
				<div class="clear height-50"></div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
<?php
$criteria=new CDbCriteria;
$criteria->with = array('description', 'category', 'brand');
$criteria->order = 'date DESC';
$criteria->addCondition('status = "1"');
$criteria->addCondition('terlaris = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $pageSize = 12;
$criteria->group = 't.id';
$dataProduct = PrdProduct::model()->findAll($criteria);

?>

	<div class="in_box_product back-white">
		<div class="tops blue text-center">
			<h3 class="sub_title">Produk Terpopular</h3>
		</div>
		<div class="prelatife container">
			<div class="insides padding-top-15">
				<div id="owl-demo2" class="lists_product_data owl-carousel owl-theme">
					<?php foreach ($dataProduct as $key => $value): ?>
						
					<div>
						<div class="items prelatife">
							<div class="t_info prelatife">
								<div class="row">
									<div class="col-xs-4 lgo_brands">
										<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(100,35, '/images/brand/'.$value->brand->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="">
									</div>
									<div class="col-xs-8">
										<div class="blocks_recom">
											<?php if (1 == $value->terbaru): ?>
											<img src="<?php echo $this->assetBaseurl ?>backs_con_right_top_products-yellow2.png" alt="">	
											<?php elseif($value->harga_coret > $value->harga): ?>
											<img src="<?php echo $this->assetBaseurl ?>backs_con_right_top_products2.png" alt="">
											<?php endif ?>
										</div>
									</div>
								</div>
							</div>
							<div class="b_titles">
								<?php echo $value->description->name ?>
							</div>
							<div class="picture">
								<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
								<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(250,250, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block">
								</a>
							</div>
							<div class="bloc_bottom">
								<div class="row">
									<div class="col-xs-9">
										<div class="prices">
											<?php if ($value->harga_coret > $value->harga): ?>
											<span class="through">Harga Normal <i><?php echo Cart::money($value->harga_coret) ?></i></span>
											<span>Kini <b><?php echo Cart::money($value->harga) ?></b></span>
											<small>Hemat <?php echo Cart::money($value->harga_coret - $value->harga) ?></small>
											<?php else: ?>
											<span class="through">&nbsp;</span>
											<span><b><?php echo Cart::money($value->harga) ?></b></span>
											<small>&nbsp;</small>
											<?php endif ?>
										</div>
									</div>
									<div class="col-xs-3 text-right">
										<?php
										$dataSerial = unserialize($value->data);
										?>
										<?php if ($dataSerial['feature'] != ''): ?>
										<i class="infree_shipping"></i>
										<?php endif ?>
										<div class="clear"></div>
										<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>" class="btn btn-link btns_blueProducts"><i class="fa fa-search"></i></a>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<?php endforeach ?>
				</div>
				<!-- end list product data -->
				<!-- <div class="text-center bs_bottom_pagin">
					<p>Halaman 1 dari 3</p>
				</div> -->
				<div class="clear height-50"></div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<!-- End box product -->
</section>

<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js" type="text/javascript" charset="utf-8" async defer></script> -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js" type="text/javascript"></script>

<script type="text/javascript">
// $.noConflict();
$(document).ready(function() {
  var owl = $("#owl-demo, #owl-demo2");
 
  owl.owlCarousel({
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0
      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
  });

  $(".play").click(function(){
    owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
  })

});
</script>
*/ ?>