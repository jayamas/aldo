<?php
$this->breadcrumbs=array(
	'Product'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-life-ring',
	'title'=>'Product',
	'subtitle'=>'Add Product',
);

$this->menu=array(
	// array('label'=>'List Product', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'input-product-form',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<div class="row-fluid">
	<div class="span12">
		<!-- ----------------- Action ----------------- -->
		<div class="widgetbox block-rightcontent">                        
		    <div class="headtitle">
		        <h4 class="widgettitle">Data File CSV</h4>
		    </div>
		    <div class="widgetcontent">

				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>'Update Data',
					'htmlOptions'=>array('class'=>'btn-large', 'value'=>'submit', 'name'=>'submit'),
				)); ?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					// 'buttonType'=>'submit',
					// 'type'=>'info',
					'url'=>CHtml::normalizeUrl(array('index')),
					'label'=>'Cancel',
					'htmlOptions'=>array('class'=>'btn-large'),
				)); ?>

			<div id="or-order-grid" class="grid-view">
			   <div class="summary"></div>
			   <table class="items table table-bordered">
			      <thead>
			         <tr>
						<th>Kode</th>
						<th>Kategori</th>
						<th>Image File Name</th>
						<th>Product Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Status</th>
						<th>Kelipatan</th>
						<th>Merek</th>
						<th>Satuan</th>
						<th>Qty Kelipatan</th>
						<!-- <th>Discount(Rp)</th>
						<th>Weight Pack</th> -->
						<!-- <th>Terpopular</th>
						<th>Terbaru</th>
						<th>Out Stock</th>
						<th>On Sale</th>
						<th>rekomendasi</th>
						<th>Turun Harga</th> -->
			         </tr>
			      </thead>
			      <tbody>
			      	<?php foreach ($dataCsv as $key => $value): ?>
			         <tr>
						<td><?php echo $value['0'] ?></td>
						<td><?php echo $value['7'] ?></td>
						<?php if ($value['6']): ?>
						<td><?php echo $value['6'].'.jpg' ?></td>
						<?php else: ?>
						<td>&nbsp;</td>
						<?php endif ?>
						<?php if ($value['1']): ?>
						<td><?php echo $value['1'] ?></td>
						<?php else: ?>
						<td>&nbsp;</td>
						<?php endif ?>
						<?php if ($value['8']): ?>
						<td><?php echo substr($value['8'], 0, 30) ?>....</td>
						<?php else: ?>
						<td>&nbsp;</td>
						<?php endif ?>
						<td><?php echo $value['4'] ?></td>
						<?php if ($value['6']): ?>
						<td>view</td>
						<?php else: ?>
						<td>&nbsp;</td>
						<?php endif ?>
						<td><?php echo $value['2'] ?></td>
						<td><?php echo $value['5'] ?></td>
						<td><?php echo $value['3'] ?></td>
						<td><?php echo $value['9'] ?></td>
			         </tr>
			      	<?php endforeach ?>
			      </tbody>
			   </table>
			</div>


				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>'Update Data',
					'htmlOptions'=>array('class'=>'btn-large', 'value'=>'submit', 'name'=>'submit'),
				)); ?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					// 'buttonType'=>'submit',
					// 'type'=>'info',
					'url'=>CHtml::normalizeUrl(array('index')),
					'label'=>'Cancel',
					'htmlOptions'=>array('class'=>'btn-large'),
				)); ?>
		    </div>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>

